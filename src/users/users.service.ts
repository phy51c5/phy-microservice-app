import { Injectable, HttpException, HttpStatus } from '@nestjs/common';

@Injectable()
export class UsersService {
    users = [{
        id: 1,
        name: 'John Doe'
    },
    {
        id: 2,
        name: 'Joe Bloggs'
    },
    {
        id: 3,
        name: 'John Q Public'
    }];
    getUsers(params) {
        if (params.id) {
            return [this.users.filter(user => (user.id === Number(params.id)))];
        }
        else {
            return this.users;
        }
    }
    postUsers(payload) {
        if (!payload.id) {
            throw new HttpException({
                status: HttpStatus.BAD_REQUEST,
                error: 'An id must be provided',
            }, HttpStatus.BAD_REQUEST);
        }
        if (!payload.name) {
            throw new HttpException({
                status: HttpStatus.BAD_REQUEST,
                error: 'Username must be provided',
            }, HttpStatus.BAD_REQUEST);
        }
        this.users.map(user => {
            if (user.id === Number(payload.id)) {
                throw new HttpException({
                    status: HttpStatus.CONFLICT,
                    error: 'User with id ' + user.id + ' alredy exists',
                }, HttpStatus.BAD_REQUEST);
            }
        });
        this.users.push(payload);
        return payload;
    }
    patchUsers(params, payload) {
        if (!Number(params.id) || params.id === 0) {
            throw new HttpException({
                status: HttpStatus.BAD_REQUEST,
                error: 'User id must be provided',
            }, HttpStatus.BAD_REQUEST);
        }
        let patchedUser = {};
        this.users.map((user, i) => {
            if (user.id === Number(params.id)) {
                Object.assign(this.users[i], payload);
                patchedUser = this.users[i];
            }
        });
        return patchedUser;
    }
    deleteUsers(params) {
        if (!Number(params.id) || params.id === 0) {
            throw new HttpException({
                status: HttpStatus.BAD_REQUEST,
                error: 'User id must be provided',
            }, HttpStatus.BAD_REQUEST);
        }
        let deletedUsers = [];
        this.users.map((user, i) => {
            if (user.id === Number(params.id)) {
                deletedUsers = this.users.splice(i, 1);
            }
        });
        return deletedUsers[0];
    }
}
