import { Controller, Get, Post, Patch, Delete, Param, Body } from '@nestjs/common';
import { MessagePattern, EventPattern } from '@nestjs/microservices';
import { UsersService } from './users.service';

interface UserDto {
    id: string;
    name: string;
};

interface Params {
    id: number;
};

@Controller('users')
export class UsersController {
    constructor(private usersService: UsersService) { }
    @Get()
    getUsers(@Param() params: Params) {
        return this.usersService.getUsers(params);
    }
    @Get(':id')
    getUser(@Param() params: Params) {
        return this.usersService.getUsers(params);
    }

    @Post()
    @MessagePattern({ name: 'John Doe' })
    createUsers(@Body() user: UserDto) {
        return this.usersService.postUsers(user);
    }

    @EventPattern('createuser')
    async handleUserCreated(data: Record<string, unknown>) {
        console.log(data);
    }

    @Patch(':id')
    patchUsers(@Param() params: Params, @Body() user: UserDto) {
        return this.usersService.patchUsers(params, user);
    }

    @Delete(':id')
    deleteUsers(@Param() params: Params) {
        return this.usersService.deleteUsers(params);
    }
}